using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] float thrustForce = 35f; 
    [SerializeField] float rotationForce = 100f;
    [SerializeField] AudioClip mainEngine;

    Rigidbody rb;
    AudioSource audioSource;

    void Start(){
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    void Update(){
        ProcessThrust();
        ProcessRotation();
    }

    void ProcessThrust(){

        if(Input.GetKey(KeyCode.Space)){
            rb.AddRelativeForce(Vector3.up * thrustForce * Time.deltaTime);
            PlayThrustAudio();
        }else{
            StopThrustAudio();
        }
    }

    void ProcessRotation(){

        if(Input.GetKey(KeyCode.A)){
           ApplyRotation(rotationForce);
        }
         else if(Input.GetKey(KeyCode.D)){
             ApplyRotation(- rotationForce);
        }

    }

    void ApplyRotation(float rotationThisFrame){
         rb.freezeRotation = true; // freezing rotation to manually rorate
         transform.Rotate(Vector3.forward * rotationThisFrame * Time.deltaTime);
         rb.freezeRotation = false; // unfreezing rotation so physics system can control
    }

    void PlayThrustAudio(){
        if(!audioSource.isPlaying){
            audioSource.PlayOneShot(mainEngine);
        }
        
    }

    void StopThrustAudio(){
        audioSource.Stop();
    }
}
